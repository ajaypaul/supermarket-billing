from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class SellerDetails(models.Model):
    gstin = models.CharField(max_length=30)
    address = models.CharField(max_length=100)
    id_card = models.CharField(max_length=50)
    registered_on = models.DateField()
    valid_till = models.DateField()


class Seller(models.Model):
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=15)
    details = models.OneToOneField(SellerDetails, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


gender_choice = [('M', 'Male'), ('F', 'Female'), ('O', 'Others')]


class Attendee(models.Model):
    name = models.CharField(max_length=100)
    image = models.FileField(upload_to='pics/')
    age = models.IntegerField()
    gender = models.CharField(choices=gender_choice, max_length=2)
    mobile = models.CharField(max_length=15)
    email = models.EmailField(blank=True)
    address = models.CharField(max_length=100, blank=True)
    department = models.ForeignKey(Department, on_delete=models.PROTECT, blank=True)

    def __str__(self):
        return self.name


class Brands(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ShelfId(models.Model):
    shelf_id = models.CharField(max_length=4)


class Shelf(models.Model):
    name = models.CharField(max_length=100)
    department = models.ForeignKey(Department, on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class Category(models.Model):
    type_of = models.CharField(max_length=100)
    life_span = models.DurationField()

    def __str__(self):
        return self.type_of


class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    expiry = models.DateField(blank=True, null=True)
    added_on = models.DateField(null=True)
    bar_code = models.CharField(unique=True, max_length=25)
    weight = models.IntegerField(blank=True, null=True)
    size = models.CharField(blank=True, max_length=5)
    category = models.ForeignKey(Category, related_name="category", on_delete=models.PROTECT)
    brand = models.ForeignKey(Brands, on_delete=models.PROTECT, related_name="brand")
    shelf = models.ForeignKey(Shelf, on_delete=models.PROTECT, related_name="shelf")
    seller = models.ForeignKey(Seller, related_name="seller", on_delete=models.PROTECT)
    shelf_code = models.ForeignKey(ShelfId, related_name="shelf_code", on_delete=models.PROTECT)

    def __str__(self):
        return "%s %s" % (self.brand, self.name)


class Denomination(models.Model):
    name = models.CharField(max_length=20)
    value = models.IntegerField(unique=True)

    def __str__(self):
        return str(self.value)


class ProductAddition(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_addition', blank=True,
                                null=True)
    quantity = models.IntegerField(default=1)
    price = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    name = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.product.name


class Cart(models.Model):
    product = models.ManyToManyField(ProductAddition, related_name='product_cart')
    amount = models.DecimalField(decimal_places=2, max_digits=10)


class BillingCounter(models.Model):
    cashier = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)
    counter_id = models.CharField(max_length=10)
    opening_balance = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.counter_id

    def cash_objects(self):
        return self.cash_set.all()


class Cash(models.Model):
    billing_counter = models.CharField(max_length=100,blank=True,null=True)
    note_type = models.ForeignKey(Denomination, on_delete=models.CASCADE, null=True)
    counts = models.IntegerField(null=True)
    date = models.DateTimeField(default=timezone.now(),editable=False)
    is_customer = models.BooleanField(default=False)
    opening = models.BooleanField(default=False)
    current = models.BooleanField(default=False)
    pay_amt = models.BooleanField(default=False)
    return_amt = models.BooleanField(default=False)
    login_unique = models.CharField(max_length=100,blank=True,null=True)
    trans_id = models.CharField(max_length=100,blank=True,null=True)
    reverted = models.BooleanField(default=False)

    def __str__(self):
        return str(self.note_type.value)

class Amount(models.Model):
    date = models.DateTimeField(default=timezone.now(),editable=False)
    login_unique = models.CharField(max_length=100,blank=True,null=True)
    amount = models.DecimalField(max_digits=10,decimal_places=2)
    opening = models.BooleanField(default=False)
    current = models.BooleanField(default=False)

    def __str__(self):
        return self.login_unique


class Transaction(models.Model):
    customer = models.CharField(max_length=100,blank=True,null=True)
    mobile = models.CharField(max_length=15,null=True)
    date = models.DateTimeField(default=timezone.now(),editable=False)


class Checkout(models.Model):
    system_used = models.ForeignKey(BillingCounter, on_delete=models.PROTECT)
    cart = models.ForeignKey(Cart, on_delete=models.PROTECT)
    date = models.DateField()
    # customer_cash = models.ForeignKey(DispensedCash, on_delete=models.PROTECT)
    customer_name = models.CharField(max_length=100, blank=True, null=True)
    customer_mob = models.CharField(max_length=15, blank=True, null=True)
    amount_return = models.ManyToManyField(Cash)



