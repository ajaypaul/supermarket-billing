# Generated by Django 2.2.2 on 2019-07-10 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0006_auto_20190709_1417'),
    ]

    operations = [
        migrations.AlterField(
            model_name='denomination',
            name='value',
            field=models.IntegerField(unique=True),
        ),
    ]
