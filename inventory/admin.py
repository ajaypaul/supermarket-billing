from django.contrib import admin
from inventory.models import *
# Register your models here.

class CashAdmin(admin.ModelAdmin):
    list_display = ['id','billing_counter','note_type','counts','date','is_customer',
                    'opening','current','pay_amt','return_amt','login_unique','trans_id','reverted']


admin.site.register(SellerDetails)
admin.site.register(ShelfId)
admin.site.register(Shelf)
admin.site.register(Seller)
admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(Category)
admin.site.register(Department)
admin.site.register(Attendee)
admin.site.register(Brands)
admin.site.register(Cash,CashAdmin)


admin.site.register(Denomination)


admin.site.register(BillingCounter)

class AmountAdmin(admin.ModelAdmin):
    list_display = ['login_unique','amount','date','opening','current']

admin.site.register(Amount,AmountAdmin)

