from django import forms
from django.forms import inlineformset_factory

from inventory.models import *

class SellerForm(forms.ModelForm):
    class Meta:
        model = Seller
        fields = "__all__"
        exclude = ['details']


    def clean(self):
        cc = self.cleaned_data
        mob = cc.get('mobile')
        print(mob)
        m = Seller.objects.filter(mobile = mob)
        print(m)
        if m:
            raise forms.ValidationError('Mobile number is already registered' , code='invalid')


class SellerDetailForm(forms.ModelForm):
    class Meta:
        model = SellerDetails
        fields = "__all__"

        widgets = {
            'registered_on':forms.DateInput(attrs={'placeholder':'yyyy-mm-dd'}),
            'valid_till':forms.DateInput(attrs={'placeholder':'yyyy-mm-dd'}),

        }

    def clean_gstin(self):
        gs = self.cleaned_data.get('gstin','')
        if gs:
            sd = SellerDetails.objects.filter(gstin = gs)
            if sd:
                raise forms.ValidationError('Gstin is already registered',code='invalid')
            return gs
        else:
            raise forms.ValidationError('please input your gstin',code='invalid')



    def clean(self):
        cc = self.cleaned_data
        reg_on = cc.get('registered_on','')
        val_till = cc.get('valid_till','')

        if reg_on =='' or val_till =='':
            raise forms.ValidationError('please check the Registered-On and Valid-Till dates',code='invalid')

        if val_till < reg_on:
            raise forms.ValidationError('Valid-Till date cannot be less than Registered-On date',code='invalid')


class DepartmentForm(forms.ModelForm):
    class Meta:
        model = Department
        fields = "__all__"

    def clean(self):
        cc = self.cleaned_data.get('name','')
        if cc:
            dep = Department.objects.filter(name = cc)
            if dep:
                raise forms.ValidationError('The Department name is already in')
        else:
            raise forms.ValidationError('Input the name of Department')


class DenominationForm(forms.ModelForm):

    class Meta:
        model = Denomination
        fields = '__all__'

class CashForm(forms.ModelForm):

    class Meta:
        model = Cash
        fields = "__all__"




class BillingForm(forms.ModelForm):

    class Meta:
        model = BillingCounter
        fields = "__all__"

class CartForm(forms.ModelForm):

    class Meta:
        model = Cart
        fields = "__all__"



class ProductAdditionForm(forms.ModelForm):
    class Meta:
        model = ProductAddition
        fields = "__all__"

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields ='__all__'


