import random

from django.forms import formset_factory
from django.http import HttpResponse, JsonResponse, Http404, HttpResponseBadRequest
from django.shortcuts import render, HttpResponseRedirect, redirect
from django.views.decorators.csrf import csrf_exempt

from inventory.forms import *
from inventory.models import *


# Create your views here.


def index(request):
    print(trans_generate())
    return render(request, 'index.html')


def seller_registration(request):
    if request.method == 'POST':
        seller = SellerForm(request.POST)
        details = SellerDetailForm(request.POST)

        if seller.is_valid() and details.is_valid():
            d = details.save()
            s = seller.save(commit=False)
            s.details = d
            s.save()
            return HttpResponseRedirect('/')
    else:
        seller = SellerForm()
        details = SellerDetailForm()

    return render(request, 'seller_registration.html', {'form_s': seller, 'form_d': details})


def department_form(request):
    if request.method == 'POST':
        form = DepartmentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = DepartmentForm()
    return render(request, 'department_form.html', {'form': form})


def cashform(request):
    if request.method == 'POST':
        form = CashForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse('/')
    else:
        form = CashForm()
    return render(request, 'cashform.html', {'form': form})


@csrf_exempt
def denomination_modal(request):
    if request.method == 'POST':
        name = request.POST.get("name", None)
        value = request.POST.get("value", None)
        data = {
            'is_taken': None
        }
        print(name, value)
        if name is not None and value is not None:
            try:
                if Denomination.objects.get(name=name) or Denomination.objects.get(value=int(value)):
                    data['is_taken'] = 'Denomination is already in list'
                    return JsonResponse(data)
            except:
                Denomination.objects.create(name=name, value=int(value))
                return JsonResponse(data)


def billing_counter_form(request):
    cashformset = inlineformset_factory(BillingCounter, Cash, form=CashForm, extra=1, min_num=1)
    if request.method == 'POST':
        form = BillingForm(request.POST)
        formset = cashformset(request.POST)
        if form.is_valid() and formset.is_valid():
            f = form.save()
            for formset in formset:
                fs = formset.save(commit=False)
                fs.billing_counter = f
                fs.save()
            return redirect('/dashboard/{}'.format(f.id))
    else:
        form = BillingForm()
        formset = cashformset()
    return render(request, 'counter_form.html', {'form': form, 'formset': formset})


def dashboard(request, id):
    try:
        billing_counter = BillingCounter.objects.get(pk=id)
        cash = billing_counter.cash_objects()
        cashobj = []
        for i in cash:
            print(i.note_type.value)
            print(i.counts)
            cashobj.append(i)

        denom = Denomination.objects.all()
        d_list = []
        for i in denom:
            d_list.append(i)
        print(d_list)
        if billing_counter:
            obj = billing_counter
            return render(request, 'dashboard.html', {'object': obj, 'denom': d_list, 'cash': cashobj})

    except:
        print("jumps")
        return Http404


def customer_payment(request, bid):

    formset = formset_factory(CashForm, extra=2)
    if request.method == "POST":
        form = formset(request.POST)
        if form.is_valid():

            try:
                bil = BillingCounter.objects.get(pk=bid)
                print(bil)
                if bil:
                    for i in form:
                        print(i)
                        f = i.save(commit=False)
                        f.billing_counter = bil
                        f.is_customer = True
                        f.save()
                    return redirect('/dashboard/{}'.format(bid))
            except:
                return Http404
    else:
        form = formset()
    return render(request, 'c_payment.html', {'form': form})






def grid(request,bill):
    code = trans_generate()
    obj = Denomination.objects.all()
    cash = Cash.objects.filter(current=True, login_unique=bill)
    if request.method == 'POST':
        note_list = request.POST.getlist('note',None)
        counts_list = request.POST.getlist("counts",None)
        return_note_list = request.POST.getlist("return_note",None)
        return_counts_list = request.POST.getlist("return_counts",None)


        payment_recieve(note_list,counts_list,bill , code)
        check=payment_return(return_note_list,return_counts_list,bill,code)
        if check:
            return redirect('/grid/{}'.format(bill))
        else:
            return redirect('/main_dash/{}'.format(bill))
    return render(request, 'grid.html', {'obj': obj,'cash':cash})



def payment_recieve(note_list , counts_list,bill,code):
    print("payment section -----------------------------")
    amount = 0
    if note_list and counts_list:
        for i in range(0, len(note_list)):
            try:
                den = Denomination.objects.get(value=int(note_list[i]))
            except:
                den = None
            if den:
                value = den.value
                cc = counts_list[i]
                if not cc == "":
                    try:
                        curr = Cash.objects.get(current=True, login_unique=bill, note_type=den)
                    except:
                        curr = None
                    cash = Cash()
                    cash.login_unique = bill
                    cash.note_type = den
                    cash.counts = int(cc)
                    cash.is_customer = True
                    cash.pay_amt = True
                    cash.trans_id=code
                    cash.save()
                    amount += value*int(cc)
                    print(cash)
                    if curr:
                        curr.counts = curr.counts + cash.counts
                        curr.save()
                    else:
                        print("ooooooo")
        print(amount)
        try:
            amt_curr = Amount.objects.get(current=True ,login_unique=bill)
        except:
            amt_curr=None

        if amt_curr:
            amt_curr.amount +=amount
            amt_curr.save()



def payment_return(note_list,counts_list,bill,code):
    print(" return section ---------------------------------")
    amount = 0
    if note_list and counts_list:
        for i in range(0,len(note_list)):
            try:
                den = Denomination.objects.get(value=int(note_list[i]))
            except:
                den = None
            if den:
                value = den.value
                cc = counts_list[i]
                print(value)
                if not cc == "":
                    try:
                        curr = Cash.objects.get(current =True,login_unique = bill,note_type=den)
                        print("here"+ str(curr.counts))
                        print(cc)
                        print(value)
                    except:
                        curr =None
                        print("its none")
                    if curr:
                        if curr.counts >= int(cc):
                            cash = Cash()
                            cash.login_unique = bill
                            cash.note_type = den
                            cash.return_amt =True
                            cash.is_customer =True
                            cash.counts=int(cc)
                            cash.trans_id = code
                            amount += value*int(cc)
                            cash.save()
                            curr.counts = curr.counts - int(cc)
                            curr.save()
                        else:
                            print("i am in else")
                            reverting_payment(bill,code)
                            return True

        print(amount)
        try:
            amt_curr = Amount.objects.get(current=True,login_unique=bill)
        except:
            amt_curr=None
        if amt_curr:
            amt_curr.amount = amt_curr.amount - amount
            amt_curr.save()



def reverting_payment(bill , code):
    print(" revert section -------------------------------------------")
    # checking the paid cash objects
    p_amount=0
    try:
        paid = Cash.objects.filter(is_customer=True,login_unique=bill,trans_id=code , pay_amt = True)
        print(paid)
    except:
        paid=None
    if paid:
        print("got the paid objects")
        for i in paid:
            note = i.note_type
            count = i.counts
            try:
                curr = Cash.objects.get(login_unique = bill,current=True,note_type = note)
                print(curr)
            except:
                curr = None
            if curr:
                curr.counts = curr.counts - count
                curr.save()
                i.reverted = True
                i.save()
                p_amount += note.value * count

        try:
            curr_amt = Amount.objects.get(current =True,login_unique=bill)
        except:
            curr_amt=None
        if curr_amt:
            curr_amt.amount = curr_amt.amount - p_amount
            curr_amt.save()

    # checking the returned objects
    r_amount = 0
    try:
        returned = Cash.objects.filter(login_unique=bill,is_customer=True,trans_id = code , return_amt = True)
        print(returned)
    except:
        returned =None
    if returned:
        for i in returned:
            note2 = i.note_type
            count2 = i.counts
            try:
                curr2 = Cash.objects.get(login_unique = bill,current=True,note_type = note2)
            except:
                curr2 = None
            if curr2:
                curr2.counts = curr2.counts + count2
                curr2.save()
                i.reverted =True
                i.save()
                r_amount += note2.value * count2
        try:
            curr_amt2 = Amount.objects.get(current =True,login_unique=bill)
        except:
            curr_amt2=None
        if curr_amt2:
            curr_amt2.amount = curr_amt2.amount + r_amount
            curr_amt2.save()






def generate():
    one = random.sample(range(1,100) , 2)
    two = random.sample(range(100,200), 2)
    three = random.sample(range(one[0],two[1]),4)
    d=""
    for i in three:
        d+=str(i)
    print(d)
    v = str(timezone.now())
    b=v.split(" ")
    print(b)
    b=b[0]
    iid = b+d
    return iid


def trans_generate():
    one = random.sample(range(200, 400), 2)
    two = random.sample(range(500, 900), 2)
    three = random.sample(range(one[0], two[1]), 4)
    d = ""
    for i in three:
        d += str(i)
    print(d)
    return d



def startup_form(request):
    obj = Denomination.objects.all()
    genn = generate()
    if request.method == 'POST':
        note_list = request.POST.getlist('note', None)
        counts_list = request.POST.getlist("counts", None)
        bill = request.POST.get('bill',None)
        print(note_list, counts_list , bill)
        amount = 0
        if note_list and counts_list and bill:

            for i in range(0, len(note_list)):
                den = Denomination.objects.get(value=int(note_list[i]))
                if den:
                    note_val = int(note_list[i])
                    cc = counts_list[i]
                    cash_o = Cash()
                    cash_c = Cash()

                    cash_o.billing_counter = bill
                    cash_o.note_type = den
                    if cc is None or cc == "":
                        cash_o.counts = 0
                    else:
                        cash_o.counts = int(cc)
                    cash_o.opening = True
                    cash_o.login_unique= bill + genn
                    cash_o.save()

                    cash_c.billing_counter = bill
                    cash_c.note_type = den
                    if cc is None or cc == "":
                        cash_c.counts = 0
                        amount += note_val * 0
                    else:
                        cash_c.counts = int(cc)
                        amount += note_val * int(cc)
                    cash_c.current = True
                    cash_c.login_unique = bill + genn
                    cash_c.save()


            request.session['unique_id'] = bill + genn
            print(request.session['unique_id'])
            a_open = Amount()
            a_current = Amount()
            a_open.login_unique = request.session['unique_id']
            a_open.amount = amount
            a_open.opening =True
            a_open.save()
            a_current.login_unique = request.session['unique_id']
            a_current.amount = amount
            a_current.current =True
            a_current.save()

            return redirect('/main_dash/{}'.format(request.session['unique_id']))

    return render(request, 'opening_form.html', {'obj': obj})


def main_dash(request,bill):
    print(request.session['unique_id'])

    cash = Cash.objects.filter(current =True ,login_unique=bill)
    try:
        open_amount= Amount.objects.get(opening=True,login_unique=bill)
        current_amount = Amount.objects.get(current=True, login_unique = bill)
    except:
        open_amount=None
        current_amount=None

    print(cash)
    if open_amount and cash and current_amount:
        profit=current_amount.amount - open_amount.amount

        return render(request,'main_dash.html',{'bill':bill,'cash':cash,'open':open_amount,
                                                'current':current_amount,'profit':profit})
    else:
        HttpResponseBadRequest

