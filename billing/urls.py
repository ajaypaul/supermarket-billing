"""billing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from inventory import urls
from inventory.views import *

urlpatterns = [

    path('admin/', admin.site.urls),
    path('', index),
    path('inventory/', include(urls)),
    path('cash_form/', cashform, name='cashform'),
    path('denomination_modal/', denomination_modal, name='d_modal'),
    path('billing_form/', billing_counter_form, name='counterform'),
    path('dashboard/<int:id>/',dashboard,name = 'dash'),
    path('dashboard/payment/<int:bid>',customer_payment,name="c_payment"),
    path('grid/<slug:bill>',grid,name ='grid'),

    path('startup_form/',startup_form,name="start_form"),
    path('main_dash/<slug:bill>/',main_dash,name='maindash'),

]
urlpatterns += [
               ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL,
                                                                                          document_root=settings.STATIC_ROOT)
